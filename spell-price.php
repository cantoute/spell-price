<?php
/*
Plugin Name: Spell Price
Description: Spell a price via shortcode [spell-price price="1234.50" currency="EURO"]
Version: 0.0.3
Plugin URI: https://www.cantoute.com/
Author: Antony GIBBS
Author URI: https://www.cantoute.com/
Text Domain: spell-price
Domain Path: /languages
*/

if (!defined('ABSPATH')) {
  header('HTTP/1.1 403 Forbidden');
  exit('HTTP/1.1 403 Forbidden');
}

add_action('init', 'spell_price_load_textdomain');

function spell_price_load_textdomain()
{
  load_plugin_textdomain('spell-price', false, dirname(plugin_basename(__FILE__)) . '/languages');
}

add_shortcode('spell-price', 'spell_price_shortcode');

function spell_price_shortcode($atts, $value)
{
  $atts = shortcode_atts(array(
    'price' => null,
    'currency' => null,
    'locale' => null,
  ), $atts);

  if ($atts['price'] === null) {
    $price = $value;
  } else {
    $price = $atts['price'];
  }

  return esc_html(spell_price($price, $atts['currency'], $atts['locale']));
}

/**
 * @param string|float $price ex: 1234.50
 * @param string $currency default 'EURO' - Ex: EURO USD CHF
 * @param string $locale ex: fr_FR
 * 
 * @return string
 */

function spell_price($price, $currency = null, $locale = null)
{
  // get_bloginfo('language');

  $return = [];

  $price = floatval(trim($price));

  if ($currency === null) {
    $currency = 'EURO';
  }

  if ($locale === null) {
    $locale = get_locale(); // fr_FR
  }

  $lang = explode('_', $locale, 1)[0]; // fr

  $currency_cent = 'cts';

  $price_int = (int) floor($price);
  // $price_cent = (int) round((int)($price * 100) - (int)($price_int * 100), 0);
  $price_cent = (int) substr(round($price * 100, 0), -2);

  $nf = new NumberFormatter($locale, NumberFormatter::SPELLOUT);
  $return[] = $nf->format($price_int);
  $return[] = ' ' . $currency;

  if ($price_cent > 0) {
    $return[] = __(' and ', 'spell-price');
    // $return[] = ', ';
    $return[] = $nf->format($price_cent);
    $return[] = ' ' . $currency_cent;
  }

  return implode('', $return);
}
