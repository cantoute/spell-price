# Spell Price

A Wordpress plugin to spell a price.

## About

This plugin is at early stage but should be functional.

### Usage

```txt

[spell-price price="1234.50" currency="EURO"]

Or

[spell-price currency="EURO"]1234.50[/spell-price]


Note that if locale attribute is omitted, it will use wordpress get_locale()

[spell-price currency="EURO" locale="fr_FR"]1234.50[/spell-price]

```

```php

/**
 * @param string|float $price ex: 1234.50
 * @param string $currency default 'EURO'
 * @param string $locale ex: fr_FR
 *
 * @return string the price in text
 */

spell_price($price, $currency = 'EURO', $locale = null)

```

#### dev notes

##### regenerate .pot

```bash
wp i18n make-pot . languages/spell-price.pot
```
