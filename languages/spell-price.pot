# Copyright (C) 2022 Antony GIBBS
# This file is distributed under the same license as the Spell Price plugin.
msgid ""
msgstr ""
"Project-Id-Version: Spell Price 0.0.3\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/spell-price\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2022-04-20T23:22:53-03:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"X-Generator: WP-CLI 2.4.0\n"
"X-Domain: spell-price\n"

#. Plugin Name of the plugin
msgid "Spell Price"
msgstr ""

#. Plugin URI of the plugin
#. Author URI of the plugin
msgid "https://www.cantoute.com/"
msgstr ""

#. Description of the plugin
msgid "Spell a price via shortcode [spell-price price=\"1234.50\" currency=\"EURO\"]"
msgstr ""

#. Author of the plugin
msgid "Antony GIBBS"
msgstr ""

#: spell-price.php:81
msgid " and "
msgstr ""
